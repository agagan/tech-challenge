#!/bin/bash

function get_metadata {
    local key=$1
    local url="http://169.254.169.254/latest/meta-data/$key"
    local value=$(curl -s $url)
    echo "\"$key\": \"$value\""
}

# Get instance metadata and format as JSON
function get_instance_metadata {
    echo "{"
    echo "  $(get_metadata ami-id),"
    echo "  $(get_metadata instance-id),"
    echo "  $(get_metadata instance-type),"
    echo "  $(get_metadata local-ipv4),"
    echo "  $(get_metadata public-ipv4),"
    echo "  $(get_metadata security-groups)"
    echo "}"
}


# Check if a key is provided as an argument
if [ "$1" ]; then
    # Call the function to get the specific key metadata
    json_output=$(get_metadata "$1")
else
    # Call the function to get instance metadata
    json_output=$(get_instance_metadata)
fi




# Print the JSON output
echo "$json_output"
terraform {
  backend "s3" {
    bucket         = "kpmg-test-backend2"
    encrypt        = true
    key            = "terraform.tfstate"
    region         = "us-east-2"
    #profile        = "garora"
    dynamodb_table = "kpmg-test-backend2"
  }
}
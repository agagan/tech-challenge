def get_value_from_nested_object(nested_object, key):
    keys = key.split('/')
    current_obj = nested_object

    try:
        for k in keys:
            current_obj = current_obj[k]
        return current_obj
    except (KeyError, TypeError):
        return None


# Example usage:
if __name__ == "__main__":
    object1 = {"a": {"b": {"c": "d"}}}
    key1 = "a/b/c"
    value1 = get_value_from_nested_object(object1, key1)
    print("Value:", value1)  # Output: Value: d

    object2 = {"x": {"y": {"z": "a"}}}
    key2 = "x/y/z"
    value2 = get_value_from_nested_object(object2, key2)
    print("Value:", value2)  # Output: Value: a
